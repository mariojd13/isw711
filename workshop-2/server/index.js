const express = require('express');
const fetch = require("node-fetch");

const app = express();

// api url
const api_url = "https://api.openbrewerydb.org/breweries?per_page=10";
// Defining async function
async function getapi(url) {

    // Storing response
    const response = await fetch(url);

    // Storing data in form of JSON
    var data = await response.json();
    console.log(data);
    if (response) {
        hideloader();
    }
    show(data);
}
// Calling that async function
getapi(api_url);

// Function to hide the loader
function hideloader() {
    document.getElementById('loading').style.display = 'none';
}

// Function to define innerHTML for HTML table
function show(data) {
    let tab =
        `<tr>
            <th>Id</th>
            <th>Name</th>
            <th>Brewery type</th>
            <th>Street</th>
            <th>City</th>
            <th>Country</th>
            <th>Phone</th>
            <th>Action</th>
            </tr>`;


    // Loop to access all rows 
    for (let r of data.list) {
        tab += `<tr> 
                <td>${r.id} </td>
                <td>${r.name}</td>
                <td>${r.brewery_type}</td> 
                <td>${r.street}</td>
                <td>${r.city}</td>
                <td>${r.country}</td> 
                <td>${r.phone}</td>
                <td><button type ="button" class="btn btn-primary">Ver más </button></td>     
            </tr>`;
    }
    // Setting innerHTML as tab variable
    document.getElementById("breweries").innerHTML = tab;
}

  //app.listen(3000, () => console.log(`Example app listening on port 3000!`))