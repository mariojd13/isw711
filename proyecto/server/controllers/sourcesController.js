const Source = require("../models/sourcesModel");
const User = require("../models/usersModel");
const Category = require("../models/categoriesModel")

/**
 * Creates a Source
 *
 * @param {*} req
 * @param {*} res
 */
const sourcePost = async (req, res) => {
  var source = new Source();

  //const user = await User.findById(req.body.id);
  const category = await Category.findById(req.body.category);

  source.url = req.body.link;
  source.name = req.body.name;
  //source.user = user;
  source.category = category;

  //console.log(category);
   if (category === null ) {

     res.status(422);
     console.log('Error while saving the category')
     res.json({
       error: 'There was an error saving the source. The category must be defined.'
     });
     return;
   }
  // console.log(source.user.name);
   (async () => {
     let feed = await parser.parseURL(source.url);
     feed.items.forEach(item =>{
      console.log(item => {
        const newsRSS = new NewsRSS({
          title: item.title,
          description: item.content,
          url: item.link,
          date: item.pubDate,
          
        })
      });
     });
   });
  console.log(req.body);

  source.save(function (err) {
    if (err) {
      res.status(422);
      console.log('Error while saving the source', err)
      res.json({
        error: 'There was an error saving the source'
      });
    } else {
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/sources/?id=${source.id}`
      });
      res.json(source);
    }

  });
};

/**
 * Get all sources
 *
 * @param {*} req
 * @param {*} res
 */
const sourceGet = (req, res) => {
  // if an specific source is required
  if (req.query && req.query.id) {
    Source.findById(req.query.id, function (err, source) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the source', err)
        res.json({ error: "Source doesnt exist" })
      }
      res.json(source);
    });
  } else {
    // get all sources
    Source.find(function (err, sources) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(sources);
    });
  }
};

/**
 * Delete one source
 *
 * @param {*} req
 * @param {*} res
 */
const sourceDelete = (req, res) => {
  // if an specific source is required
  if (req.query && req.query.id) {
    Source.findById(req.query.id, function (err, source) {
      if (err) {
        res.status(500);
        console.log('Error while queryting the source', err)
        res.json({ error: "Source doesnt exist" })
      }
      //if the source exists
      if (source) {
        source.remove(function (err) {
          if (err) {
            res.status(500).json({ message: "There was an error deleting the source" });
          }
          res.status(204).json({});
        })
      } else {
        res.status(404);
        console.log('Error while queryting the source', err)
        res.json({ error: "Source doesnt exist" })
      }
    });
  } else {
    res.status(404).json({ error: "You must provide a source id" });
  }
};

/**
 * Updates a source
 *
 * @param {*} req
 * @param {*} res
 */
const sourcePatch = (req, res) => {
  // get source by id
  if (req.query && req.query.id) {
    Source.findById(req.query.id, function (err, source) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the source', err)
        res.json({ error: "Source doesnt exist" })
      }

      // update the source object (patch)
      source.url = req.body.url ? req.body.url : source.url;
      source.name = req.body.name ? req.body.name : source.name;
      //source.user_id = req.body.user_id ? req.body.user_id : source.user_id;
      //source.category_id = req.body.category_id ? req.body.category_id : source.category_id;

      source.save(function (err) {
        if (err) {
          res.status(422);
          console.log('Error while saving the source', err)
          res.json({
            error: 'There was an error saving the source'
          });
        }
        res.status(200); // OK
        res.json(source);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "Source doesnt exist" })
  }
};

module.exports = {
  sourceGet,
  sourcePost,
  sourcePatch,
  sourceDelete,
}