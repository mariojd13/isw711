const express = require('express');
const app = express();
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/todo-api");

const {
  taskPatch,
  taskPost,
  taskGet,
  taskDelete
} = require("./controllers/taskController.js");

const {
  studentPatch,
  studentPost,
  studentGet,
  studentDelete
} = require("./controllers/studentController.js");

const {
  coursePatch,
  coursePost,
  courseGet,
  courseDelete
} = require("./controllers/courseController.js");

const {
  userPatch,
  userPost,
  userGet,
  userDelete,
  userSession,
} = require("./controllers/userController.js");

const {
  rolPatch,
  rolPost,
  rolGet,
  rolDelete
} = require("./controllers/rolController.js");

const {
  categoryPatch,
  categoryPost,
  categoryGet,
  categoryDelete
} = require("./controllers/categoryController");


const {
  sourcePatch,
  sourcePost,
  sourceGet,
  sourceDelete
} = require("./controllers/sourcesController");

const {
  noticePatch,
  noticePost,
  noticeGet,
  noticeDelete
} = require("./controllers/newController");

// parser for the request body (required for the POST and PUT methods)
const bodyParser = require("body-parser");
app.use(bodyParser.json());

// check for cors
const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));


// listen to the task request
app.get("/api/tasks", taskGet);
app.post("/api/tasks", taskPost);
app.patch("/api/tasks", taskPatch);
app.put("/api/tasks", taskPatch);
app.delete("/api/tasks", taskDelete);

app.get("/api/students", studentGet);
app.post("/api/students", studentPost);
app.patch("/api/students", studentPatch);
app.put("/api/students", studentPatch);
app.delete("/api/students", studentDelete);

app.get("/api/courses", courseGet);
app.post("/api/courses", coursePost);
app.patch("/api/courses", coursePatch);
app.put("/api/courses", coursePatch);
app.delete("/api/courses", courseDelete);

app.get("/api/users", userGet);
app.post("/api/users", userPost);
app.post("/api/users/login", userSession);
app.patch("/api/users", userPatch);
app.put("/api/users", userPatch);
app.delete("/api/users", userDelete);

app.get("/api/roles", rolGet);
app.post("/api/roles", rolPost);
app.patch("/api/roles", rolPatch);
app.put("/api/roles", rolPatch);
app.delete("/api/roles", rolDelete);

app.get("/api/categories", categoryGet);
app.post("/api/categories", categoryPost);
app.patch("/api/categories", categoryPatch);
app.put("/api/categories", categoryPatch);
app.delete("/api/categories", categoryDelete);

app.get("/api/sources", sourceGet);
app.post("/api/sources", sourcePost);
app.patch("/api/sources", sourcePatch);
app.put("/api/sources", sourcePatch);
app.delete("/api/sources", sourceDelete);

app.get("/api/notices", noticeGet);
app.post("/api/notices", noticePost);
app.patch("/api/notices", noticePatch);
app.put("/api/notices", noticePatch);
app.delete("/api/notices", noticeDelete);


app.listen(3000, () => console.log(`Example app listening on port 3000!`))
