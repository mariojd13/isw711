const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const user = require('../models/usersModel');
const category = require('../models/categoriesModel');



const source = new Schema({
  url: { type: String },
  name: { type: String },
  user: user.schema,
  category: category.schema
});

module.exports = mongoose.model('sources', source);