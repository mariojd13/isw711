const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const rol = require('../models/rolesModel');


const user = new Schema({
  email: {type: String},
  password: {type : String},
  first_name: {type: String},
  last_name: {type: String},
  address1: {type: String},
  address2: {type: String, require: false},
  country: {type: String},
  city: {type: String},
  number: {type: String},
  rol: rol.schema

});

module.exports = mongoose.model('users', user);