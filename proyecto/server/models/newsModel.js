const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = require('../models/usersModel');
const source = require('../models/sourcesModel');
const category = require('../models/categoriesModel');

const notice = new Schema({
  title: { type: String },
  description: { type: String },
  permanlink: { type: String },
  date:{type: Date, default: Date.now},
  user: user.schema,
  source: source.schema,
  category: category.schema

});

module.exports = mongoose.model('notices', notice);