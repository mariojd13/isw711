import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Login from '../pages/Login';
import Index from '../pages/Index';
import NewUsers from '../pages/NewUsers';
import Categories from '../pages/Categories';
import Source from '../pages/Source';
import news from '../pages/news';
//import Footer from '../comun/Footer';

function Routes() {
  return (

    <BrowserRouter>
      <Switch>
        <Route exact path = "/" component={Login}/>
        <Route exact path = "/index" component={Index}/>
        <Route exact path = "/newusers" component={NewUsers}/>
        <Route exact path = "/categories" component={Categories}/>
        <Route exact path = "/sources" component={Source}/>
        <Route exact path = "/news" component={news}/>
      </Switch>
    </BrowserRouter>

  );
}

export default Routes;
