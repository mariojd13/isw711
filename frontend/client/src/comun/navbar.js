import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Cookies from 'universal-cookie/es6';

import { Navbar, Container, Nav, NavDropdown, DropdownButton, Dropdown } from 'react-bootstrap';

const cookies = new Cookies();

class NavbarCover extends Component {

  closeSesion = () => {
    cookies.remove('id', { path: "/" });
    cookies.remove('email', { path: "/" });
    cookies.remove('first_name', { path: "/" });
    cookies.remove('last_name', { path: "/" });
    cookies.remove('address1', { path: "/" });
    cookies.remove('address2', { path: "/" });
    cookies.remove('country', { path: "/" });
    cookies.remove('city', { path: "/" });
    cookies.remove('number', { path: "/" });
    cookies.remove('rol', { path: "/" });
    window.location.href = ('./')
  }

  
  render() {
    return (
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#home">My Cover</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="/index">My Cover</Nav.Link>
              <Nav.Link href="/categories">Categorías</Nav.Link>
              <Nav.Link href="/sources">Recursos</Nav.Link>
            </Nav>
          </Navbar.Collapse>
          <NavDropdown title={cookies.get('first_name')} id="basic-nav-dropdown">
            <NavDropdown.Item onClick={() => this.closeSesion()}>Logout</NavDropdown.Item>
          </NavDropdown>
        </Container>
      </Navbar >
    )
  }
}

export default NavbarCover;