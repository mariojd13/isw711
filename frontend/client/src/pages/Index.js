import React, { Component } from 'react';
import "../css/Index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Dropdown, DropdownButton } from 'react-bootstrap';
import axios from 'axios';
import Cookies from 'universal-cookie/es6';
import NavbarCover from '../comun/navbar';
const cookies = new Cookies();
//npm install react-bootstrap bootstrap@5.0.2
const baseUrl="http://localhost:3000/api/";

class Index extends Component {

    state = {
        notices: [],
        categories: [],
        selectedCat: ""
    };

    componentDidMount() {
        /*if(!cookies.get('email')){
            window.location.href=("./")
        }*/
        axios.get(baseUrl+"notices").then(response => {
            this.setState({ notices: response.data });
        }).catch(error => {
            console.log(error.message);
        })
        axios.get(baseUrl+"categories").then(response => {
            this.setState({ categories: response.data });
        }).catch(error => {
            console.log(error.message);
        })
    }

    render() {
        console.log('id: ' + cookies.get('id'));
        console.log('email: ' + cookies.get('email'));
        console.log('first_name: ' + cookies.get('first_name'));
        console.log('last_name: ' + cookies.get('last_name'));
        console.log('address1: ' + cookies.get('address1'));
        console.log('address2: ' + cookies.get('address2'));
        console.log('country: ' + cookies.get('country'));
        console.log('city: ' + cookies.get('city'));
        console.log('number: ' + cookies.get('number'));
        console.log('rol: ' + cookies.get('rol'));
        return (
            <div>
                <NavbarCover />
                <br />
                <div className="conteinerPrincipal">

                    <div className="row col">
                        <div className="col">
                        </div>
                        <div className="col">
                        </div>
                    </div>

                    <div className="conteinerSecundario">
                        <div className="btn-group-header" role="group" aria-label="Basic outlined example">
                            {this.state.categories.map((cat, index) => {
                                return(
                                    <button key={index} onClick={()=>{this.setState({selectedCat:cat.name})}} type="button" className="btn btn-outline-primary">{cat.name}</button>                            
                                )
                            }
                            )}
                        </div>
                        <div className="row">
                        {this.state.notices.map((news, index) => {
                            if(news.category.name == this.state.selectedCat || this.state.selectedCat=="" ){
                                return(
                                    <div key={index} className="col-sm-6">
                                        <div className="card">
                                            <div className="card-body">
                                                <h5 className="card-title">{news.title}</h5>
                                                <p className="card-text">{news.description}</p>
                                                <a href={news.permanlink} target="_blank" className="btn btn-primary">Ver Noticia</a>
                                            </div>
                                        </div>
                                    </div>
                                );
                            }
                        } 
                        )}

                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default Index;
