import React, { Component } from 'react';
//import logo from './logo.svg';
import "../css/CRUDCategories.css";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faLaptopHouse, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import NavbarCover from '../comun/navbar';

const url = "http://localhost:3000/api/categories/";

class Categories extends Component {

    state = {
        data: [],
        addModal: false,
        modalDelate: false,
        form: {
            id: '',
            name: '',
            modalType: ''
        }
    }

    handleChange=async e=>{
        e.persist();
        await this.setState({
          form:{
            ...this.state.form,
            [e.target.name]: e.target.value
          }
        });
        console.log(this.state.form);
        }

    getCategories = () => {
        axios.get(url).then(response => {
            this.setState({ data: response.data });
        }).catch(error => {
            console.log(error.message);
        })
    }

    postCategories = async () => {
        await axios.post(url, this.state.form).then(response => {
            this.addModal();
            this.getCategories();
        }).catch(error => {
            console.log(error.message);
        })
    }

    putCategories = () => {
        console.log(url,this.state.form.id);
        axios.put(url + '?id=' + this.state.form.id, this.state.form).then(response => {
            this.addModal();
            this.getCategories();
        })
    }

    delateCategories = () => {
        axios.delete(url + '?id=' + this.state.form.id).then(response => {
            this.setState({ modalDelate : false });
            this.getCategories();
        })
    }

    addModal = () => {
        this.setState({ addModal: !this.state.addModal });
    }



    selectCateories = (categories) => {
        console.log(categories);
        this.setState({
            modalType: 'update',
            form: {
                id: categories._id,
                name: categories.name,
            }
        })
    }

    componentDidMount() {
        this.getCategories();
    }

    render() {
        const { form } = this.state;
        //const form = this.state.form;
        return (
            <div className="App">
                <NavbarCover />
                <br /><br /><br />
                <button className="btn btn-success" onClick={() => { this.setState({ form: null, modalType: 'add' }); this.addModal() }}>Agregar Categoría</button>
                <br /><br />
                <table className="table ">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map(categories => {
                            return (
                                <tr>
                                    <td>{categories.name}</td>
                                    <td>
                                        <button className="btn btn-primary" onClick={() => { this.selectCateories(categories); this.addModal() }}><FontAwesomeIcon icon={faEdit} /></button>
                                        {"              "}
                                        <button className="btn btn-danger" onClick={() => { this.selectCateories(categories); this.setState({ modalDelate: true }) }}><FontAwesomeIcon icon={faTrashAlt} /></button>                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>

                <Modal isOpen={this.state.addModal}>
                    <ModalHeader style={{ display: 'block' }}>
                        <span style={{ float: 'right' }} onClick={() => this.addModal()}>X</span>
                    </ModalHeader>
                    <ModalBody>
                        <div className="form-group">
                            <label htmlFor="id">ID</label>
                            <input className="form-control" type="text" name="id" id="id" readOnly onChange={this.handleChange} value={form ? form.id : this.state.data.length + 1} />
                            <br />
                            <label htmlFor="name">Nombre</label>
                            <input className="form-control" type="text" name="name" id="name" onChange={this.handleChange} value={form ? form.name : ''} />
                            <br />
                        </div>
                    </ModalBody>

                    <ModalFooter>
                        {this.state.modalType == 'add' ?
                            <button className="btn btn-success" onClick={() => this.postCategories()}>
                                Insertar
                            </button> : <button className="btn btn-primary" onClick={() => this.putCategories()}>
                                Actualizar
                            </button>
                        }
                        <button className="btn btn-danger" onClick={() => this.addModal()}>Cancelar</button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.modalDelate}>
                    <ModalBody>
                        Estás seguro que deseas eliminar a la la categoría
                    </ModalBody>
                    <ModalFooter>
                        <button className="btn btn-danger" onClick={() => this.delateCategories()}>Sí</button>
                        <button className="btn btn-secundary" onClick={() => this.setState({ modalDelate: false })}>No</button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}
export default Categories;