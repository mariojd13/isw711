import React, { Component } from 'react';
//import logo from './logo.svg';
import "../css/CRUDCategories.css";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faLaptopHouse, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import NavbarCover from '../comun/navbar';
import Categories from './Categories';

const url = "http://localhost:3000/api/sources/";
const url2 = "http://localhost:3000/api/categories/";

class Sources extends Component {
  state = {
    data: [],
    addModal: false,
    modalDelate: false,

    form: {
      id: '',
      name: '',
      url: '',
      //user: '',
      modalType: ''
    },
    categories: [],
  };

  handleChange = async (e) => {
    e.persist();
    await this.setState({
        form: {
            ...this.state.form,
            [e.target.name]: e.target.value
        }
    });
    console.log(this.state.form.category_id);
  };

  handleChange2 = async (e) => {
    e.persist();
    await this.setState({
      form: {
        ...this.state.form,
        category: e.target.value,
      },
    });
    console.log(this.state.form);
  };

  getCategories = () => {
    axios.get(url2).then(response => {
      this.setState({ categories: response.data });
    }).catch(error => {
          console.log(error.message);
      })
    };

  getSources = () => {
    axios.get(url).then(response => {
      this.setState({ data: response.data });
    }).catch(error => {
      console.log(error.message);
    })
  };

  postSources = async () => {
    await axios.post(url, this.state.form).then(response => {
      this.addModal();
      this.getSources();
    }).catch(error => {
        console.log(error.message);
    })
  }

  putSources = () => {
    console.log(url, this.state.form.id);
      axios.put(url + '?id=' + this.state.form.id, this.state.form).then(response => {
        this.addModal();
        this.getSources();
      })
  }

  delateSources = () => {
    axios.delete(url + "?id=" + this.state.form.id).then((response) => {
      this.setState({ modalDelate: false });
      this.getSources();
    });
  }

  addModal = () => {
    this.setState({ addModal: !this.state.addModal });
  };

  selectCateories = (sources) => {
    console.log(sources);
    this.setState({
      modalType: 'update',
      form: {
        id: sources._id,
        name: sources.name,
        url: sources.url,
        category: sources.category.name,
      }
    })
  }

  componentDidMount() {
    this.getSources();
    this.getCategories();
  }

  render() {
    const { form } = this.state;
    //const form = this.state.form;
    return (
      <div className="App">
        <NavbarCover />
        <br /><br /><br />
        <button className="btn btn-success" onClick={() => { this.setState({ form: null, modalType: 'add' }); this.addModal() }}>Agregar Categoría</button>
        <br /><br />
        <table className="table ">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Categoría</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {this.state.data.map((sources) => {
              return (
                <tr>
                  <td>{sources.name}</td>
                  <td>{sources.category.name}</td>
                  <td>
                    <button className="btn btn-primary" onClick={() => { this.selectCateories(sources); this.addModal() }}><FontAwesomeIcon icon={faEdit} /></button>
                    {"              "}
                    <button className="btn btn-danger" onClick={() => { this.selectCateories(sources); this.setState({ modalDelate: true }) }}><FontAwesomeIcon icon={faTrashAlt} /></button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>

        <Modal isOpen={this.state.addModal}>
        <ModalHeader style={{ display: 'block' }}>
            <span style={{ float: 'right' }} onClick={() => this.addModal()}>X</span>
        </ModalHeader>
          <ModalBody>
            <div className="form-group">
              <label htmlFor="name">Nombre</label>
              <input className="form-control" type="text" name="name" id="name" onChange={this.handleChange} value={form ? form.name : ''} />
              <br />
              <label htmlFor="url">URL</label>
              <input className="form-control" type="text" name="url" id="url" onChange={this.handleChange} value={form ? form.url : ''} />
              <br />
              <label htmlFor="category_id">Categoría</label>
              <select value={this.state.category_id} onChange={this.handleChange2} value={form ? form.category_id : ''}>
                  {
                      this.state.categories.map(category => <option key={category._id} value={category._id}>{category.name} </option> )
                  }
              </select>
              <br />
            </div>
          </ModalBody>

          <ModalFooter>
            {this.state.modalType == 'add' ?
              <button className="btn btn-success" onClick={() => this.postSources()}>
                  Insertar
              </button> : <button className="btn btn-primary" onClick={() => this.putSources()}>
                  Actualizar
              </button>
            }
            <button className="btn btn-danger" onClick={() => this.addModal()}>Cancelar</button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modalDelate}>
        <ModalBody>
            Estás seguro que deseas eliminar a la la categoría
        </ModalBody>
        <ModalFooter>
            <button className="btn btn-danger" onClick={() => this.delateSources()}>Sí</button>
            <button className="btn btn-secundary" onClick={() => this.setState({ modalDelate: false })}>No</button>
        </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default Sources;