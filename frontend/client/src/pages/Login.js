import React, { Component } from "react";
import "../css/Login.css";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
//import md5 from 'md5';
import Cookies from "universal-cookie/es6";

const baseUrl = "http://localhost:3000/api/users/login";
const cookies = new Cookies();

class Login extends Component {
  state = {
    email: "",
    password: "",
  };

  //Método para capturar lo que el usuario escribe en los inputs
   handleChangeEmail = async (e) => {
    e.persist();
     await this.setState({
      email:e.target.value
     })
  };

  handleChangePassword = async (e) => {
    e.persist();
    await this.setState({
     password:e.target.value
    })
 };

  //Método para petición http de iniciar sesión
  sesionStar = async () => {
    //console.log(this.state.form)
    //Se consulta la base de datos, con el método get, y se le pasan los parámetros
    await axios
      .post(baseUrl, {
          email: this.state.email,
          password: this.state.password,
        
      })
      //Si usamos el MD5 para el sifrado de la contraseña
      //await axios.get(baseUrl,{params: {email: this.state.form.email, password: md5(this.state.form.password)}})
      
      .then((response) => {
        return response.data;
      })
      .then((response) => {

       
          var respuesta = response;
          cookies.set("id", respuesta._id, { path: "/" });
          cookies.set("email", respuesta.email, { path: "/" });
          cookies.set("first_name", respuesta.first_name, { path: "/" });
          cookies.set("last_name", respuesta.last_name, { path: "/" });
          cookies.set("address1", respuesta.address1, { path: "/" });
          cookies.set("address2", respuesta.address2, { path: "/" });
          cookies.set("country", respuesta.country, { path: "/" });
          cookies.set("city", respuesta.city, { path: "/" });
          cookies.set("number", respuesta.number, { path: "/" });
          cookies.set("rol", respuesta.rol, { path: "/" });
          alert(`Bienvenido ${respuesta.first_name} ${respuesta.last_name}`);
          window.location.href = "./index";
      })
      .catch((error) => {
        alert("El usuario o contraseña no son correctos");
        console.log(error);
      });
  };

  componentDidMount() {
    if (cookies.get("email")) {
      window.location.href = "./index";
    }
  }

  render() {
    return (
      <div className="conteinerPrincipal">
        <div className="conteinerSecundario">
          <h2>Login</h2>
          <div className="from-group">
            <label>Email:</label>
            <br />
            <input
              type="text"
              className="from-control"
              name="email"
              onChange={this.handleChangeEmail}
            ></input>
            <br />
            <label>Password:</label>
            <br />
            <input
              type="password"
              className="from-control"
              name="password"
              onChange={this.handleChangePassword}
            ></input>
            <br />
            <br />
            <button
              className="btn btn-primary"
              onClick={() => this.sesionStar()}
            >
              Login
            </button>
            <br />
          </div>
        </div>
        <div className="conteinerSecundario">
          <span>Forgot Password?</span>
          <br />
          <span>Not a user? </span>
          <a href="../NewUsers">Sign Up</a>
        </div>
      </div>
    );
  }
}

export default Login;
